#include "Room.h"
#include <algorithm>


Room::Room(int id, User * admin, std::string name, int maxUsers, int questionsNo, int questionTime)
{
	_id = id;
	_admin = admin;
	_name = name;
	_maxUsers = maxUsers;
	_questionsNo = questionsNo;
	_questionTime = questionTime;

	_users.push_back(admin);
}

bool Room::joinRoom(User* user)
{
	bool isJoin = false;
	std::string massage = "110";
	if (_users.size() == _maxUsers)
	{
		massage += "1";
		user->send(massage);
	}
	else
	{
		isJoin = !isJoin;
		
		_users.push_back(user);

		massage += "0" + Helper::getPaddedNumber(_questionsNo, 2) + Helper::getPaddedNumber(_questionTime, 2);
		user->send(massage);
		
		sendMessage(getUsersListMessage());
	}
	return isJoin;
}

void Room::leaveRoom(User* user)
{
	std::vector<User*>::iterator it;
	bool isExist = false;
	for (it = _users.begin(); it != _users.end(); ++it)
	{
		if ((*it)->getUsername() == user->getUsername())
		{
			isExist = !isExist;
			break;
		}
	}
	if (isExist)
	{
		_users.erase(std::remove(_users.begin(), _users.end(), user), _users.end());
		user->send("1120");

		sendMessage(getUsersListMessage());
	}
}

int Room::closeRoom(User * user)
{
	int id;
	if (user->getUsername() == _admin->getUsername())
	{
		sendMessage("116");
		id = _id;
		
		std::vector<User*>::iterator it;
		for (it = _users.begin(); it != _users.end(); ++it)
		{
			if ((*it)->getUsername() != user->getUsername())
				(*it)->clearRoom();
		}
	}
	else
	{
		id = -1;
	}
	return id;
}

std::string Room::getUsersAsString(std::vector<User*> usersList, User* excludeUser)
{
	std::vector<User*>::iterator it;
	std::string usersString;
	for (it = usersList.begin(); it != usersList.end(); ++it)
	{
		if((*it)->getUsername() != excludeUser->getUsername())
		usersString += (*it)->getUsername() + ", ";
	}
	return usersString;
}

void Room::sendMessage(std::string message)
{
	sendMessage(NULL, message);
}

void Room::sendMessage(User* excludeUser, std::string message)
{
	std::vector<User*>::iterator it;
	for (it = _users.begin(); it != _users.end(); ++it)
	{
		if(excludeUser != NULL && (*it)->getUsername() != excludeUser->getUsername())
			(*it)->send(message);
		else if (excludeUser == NULL)
			(*it)->send(message);
	}
}

std::vector<User*> Room::getUsers()
{
	return _users;
}

std::string Room::getUsersListMessage()
{
	std::string message = "108" + std::to_string(_users.size());
	std::vector<User*>::iterator it;
	for (it = _users.begin(); it != _users.end(); ++it)
	{
		message +=  Helper::getPaddedNumber((*it)->getUsername().length(), 2) + (*it)->getUsername();
	}
	return message;
}

int Room::getQuestionsNo()
{
	return _questionsNo;
}

int Room::getId()
{
	return _id;
}

std::string Room::getName()
{
	return _name;
}

//#pragma comment (lib, "ws2_32.lib")
#include "TriviaServer.h"
#include <iostream>
#include <fstream>

#include <cstdlib>

void main()
{
	srand(time(NULL));
	try
	{
		TRACE("Starting...");
		
		WSADATA wsa_data = {};
		if (WSAStartup(MAKEWORD(2, 2), &wsa_data) != 0)
			throw std::exception("WSAStartup Failed");
		
		TriviaServer server;
		server.serve();
	}
	catch (const std::exception& e)
	{
		std::cout << "Exception was thrown in function: " << e.what() << std::endl;
	}
	catch (...)
	{
		std::cout << "Unknown exception in main !" << std::endl;
	}
	system("pause");
}
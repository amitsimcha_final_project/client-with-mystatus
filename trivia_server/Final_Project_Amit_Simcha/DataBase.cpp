﻿#include "DataBase.h"

// globle veriables
int rc;
char *zErrMsg = 0;
std::string data; // each time callbackData is used the variable data will contain a diffrant value 
std::string query; // a query, to ask the data base for data 
std::vector<std::string> questionsFromCallBack; // a vector of strings, each 6 strings is a question
std::vector<std::string> personalAnswersFromCallBack; // a vector of strigs, each 6 strings is an answer in a game thet the player attended
std::vector<std::string> infoAboutUsersFromCallBack; // a vector thet will contain the user names and ther scores
const std::string quoteSign = "\""; // to deal with the issue that sqlite can't get a string witout quoting signs ("") 
int counter = 0; // a counter for the callBackCount function

// call back function to get a spacific data from the data base
int callbackData(void* notUsed, int argc, char** argv, char** azCol)
{
	data = argv[0];
	return 0;
}

DataBase::DataBase()
{
	// connection to the database
	rc = sqlite3_open("trivia_db.db", &_db);

	// if dosnt work need to throw an excption
	if (rc != SQLITE_OK)
		throw std::exception(sqlite3_errmsg(_db));
}

DataBase::~DataBase()
{
	// close the db
	sqlite3_close(_db);
}

bool DataBase::isUserExists(std::string username)
{
	bool isExist = false;

	// convert the string to a string with quotes
	std::string quoted_username = quoteSign + username + quoteSign;

	query = "select username from t_users where username = " + quoted_username + ";";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);

	if (data == username) // if returns an error so that means thet the user doesn't exist
	{
		// the user dose excist
		isExist = true;
	}

	data.clear();

	return isExist;
}

bool DataBase::addNewUser(std::string username, std::string password, std::string email)
{
	bool isAdd = false;

	// convert the string to a string with quotes
	std::string quoted_username = quoteSign + username + quoteSign;
	std::string quoted_password = quoteSign + password + quoteSign;
	std::string quoted_email = quoteSign + email + quoteSign;

	query = "insert into t_users(username, password, email) values(" + quoted_username + "," + quoted_password + "," + quoted_email + ");";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc != SQLITE_OK) // ther was a fail in the adding of the user
	{
		std::cout << "SQL error: " << zErrMsg << std::endl;
		sqlite3_free(zErrMsg);
		system("Pause");
	}
	else // the user have been added
	{
		isAdd = true;
	}
	return isAdd;
}

bool DataBase::isUserAndPassMatch(std::string username, std::string password)
{
	bool isUserAndPassMatch = true;
	std::string dataFromTable[2];

	// convert the string to a string with quotes
	std::string quoted_username = quoteSign + username + quoteSign;
	std::string quoted_password = quoteSign + password + quoteSign;

	query = "select password from t_users where username = " + quoted_username + ";";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (data == password)
	{
		isUserAndPassMatch = true;
	}
	else // means that he not found a metching password
	{
		isUserAndPassMatch = false;
	}

	data.clear();

	return isUserAndPassMatch;
}

std::vector<Question*> DataBase::initQuestions(int questionsNo)
{
	// hey AMIT, I think that you missing the poenta of the random... tock to me

	srand((unsigned)time(0));

	std::string tempStringQuestion[6] = { " ", " ", " ", " ", " ", " " };
	std::vector<Question*> randomVecOfQuestions;

	query = "select * from t_questions;";

	rc = sqlite3_exec(_db, query.c_str(), callbackQuestions, 0, &zErrMsg);


	int i, j;

	for (i = 0, j = 0; j < questionsNo; i += 6, j++)
	{
		tempStringQuestion[0] = questionsFromCallBack[i];     // question_id
		tempStringQuestion[1] = questionsFromCallBack[i + 1]; // question
		tempStringQuestion[2] = questionsFromCallBack[i + 2]; // correct_ans
		tempStringQuestion[3] = questionsFromCallBack[i + 3]; // ans2
		tempStringQuestion[4] = questionsFromCallBack[i + 4]; // ans3
		tempStringQuestion[5] = questionsFromCallBack[i + 5]; // ans4

															  // create a new question
		Question* tempQuestion = new Question
		(
			std::stoi(tempStringQuestion[0]),
			tempStringQuestion[1],
			tempStringQuestion[2],
			tempStringQuestion[3],
			tempStringQuestion[4],
			tempStringQuestion[5]
		);

		// add the new question to the vector that will be returnd
		randomVecOfQuestions.push_back(tempQuestion);
	}

	// shuffle the vector to make it randomaized
	std::random_shuffle(randomVecOfQuestions.begin(), randomVecOfQuestions.end());

	return randomVecOfQuestions;
}

int DataBase::insertNewGame()
{
	/*int roomId = 0;

	// convert the string to a string with quotes
	std::string quoted_NOW = quoteSign + "NOW" + quoteSign;
	std::string quoted_NULL = quoteSign + "NULL" + quoteSign;

	query = "insert into t_games(status, start_time, end_time) values(0," + quoted_NOW + "," + quoted_NULL + ");";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		// the insertion was susuccfull
		roomId = (int)sqlite3_last_insert_rowid(_db);
	}

	data.clear();

	return roomId;*/
	int roomId = 0;

	std::string curr_time;
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	// initializing the current time into a string
	curr_time = std::to_string(tm.tm_hour) + ":" + std::to_string(tm.tm_min) + ":" + std::to_string(tm.tm_sec);

	std::string curr_date;

	// initializing the current date into a string
	curr_date = std::to_string(tm.tm_year + 1900) + "-" + std::to_string(tm.tm_mon + 1) + "-" + std::to_string(tm.tm_mday);

	std::string full_curr_time = curr_date + " " + curr_time;

	// convert the string to a string with quotes
	std::string quoted_full_curr_time = quoteSign + full_curr_time + quoteSign;
	std::string quoted_NULL = quoteSign + "NULL" + quoteSign;

	query = "insert into t_games(status, start_time, end_time) values(0," + quoted_full_curr_time + "," + quoted_NULL + ");";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		// the insertion was susuccfull
		roomId = (int)sqlite3_last_insert_rowid(_db);
	}

	return roomId;
}

bool DataBase::updateGameStatus(int gameId)
{
	/*bool isUpdate = false;
	// convert the gameId(int) to a gameId(string)
	std::string str_gameId;
	std::ostringstream convert;
	convert << gameId;
	str_gameId = convert.str();

	// convert the string to a string with quotes
	std::string quoted_NOW = quoteSign + "NOW" + quoteSign;

	query = "update t_games set status = 1, end_time = " + quoted_NOW + " where game_id = " + str_gameId + ";";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		isUpdate = true;
	}

	data.clear();

	convert.str("");
	convert.clear(); // clear state flags

	return isUpdate;*/

	bool isUpdate = false;

	// convert the gameId(int) to a gameId(string)
	std::string str_gameId;
	std::ostringstream convert;
	convert << gameId;
	str_gameId = convert.str();

	std::string curr_time;
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	// initializing the current time into a string
	curr_time = std::to_string(tm.tm_hour) + ":" + std::to_string(tm.tm_min) + ":" + std::to_string(tm.tm_sec);

	std::string curr_date;

	// initializing the current date into a string
	curr_date = std::to_string(tm.tm_year + 1900) + "-" + std::to_string(tm.tm_mon + 1) + "-" + std::to_string(tm.tm_mday);

	std::string full_curr_time = curr_date + " " + curr_time;

	// convert the string to a string with quotes
	std::string quoted_full_curr_time = quoteSign + full_curr_time + quoteSign;

	query = "update t_games set status = 1, end_time = " + quoted_full_curr_time + " where game_id = " + str_gameId + ";";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		isUpdate = true;
	}
	return isUpdate;
}

bool DataBase::addAnswerToPlayer(int gameId, std::string username, int questionId, std::string answer, bool isCorrect, int answerTime)
{
	bool isAdd = false;

	// convert gameId(int) to gameId(string)
	std::string str_gameId;
	std::ostringstream convert;
	convert << gameId;
	str_gameId = convert.str();

	convert.str("");
	convert.clear(); // Clear state flags.

					 // convert questionId(int) to questionId(string)
	std::string str_questionId;
	convert << questionId;
	str_questionId = convert.str();

	convert.str("");
	convert.clear(); // Clear state flags.

					 // convert isCorrect(bool(0 = false 1 = true)) to isCorrect(string)
	std::string str_isCorrect;
	convert << isCorrect;
	str_isCorrect = convert.str();

	convert.str("");
	convert.clear(); // Clear state flags.

					 // convert answerTime(int) to answerTime(string)
	std::string str_answerTime;
	convert << answerTime;
	str_answerTime = convert.str();

	convert.str("");
	convert.clear(); // Clear state flags.

					 // convert the string to a string with quotes
	std::string quoted_username = quoteSign + username + quoteSign;
	std::string quoted_answer = quoteSign + answer + quoteSign;

	std::string values = str_gameId + "," + quoted_username + "," + str_questionId + "," + quoted_answer + "," + str_isCorrect + "," + str_answerTime;

	// adds the answer to the player
	query = "insert into t_players_answers(game_id, username, question_id, player_answer, is_correct, answer_time) values(" + values + ");";

	rc = sqlite3_exec(_db, query.c_str(), callbackData, 0, &zErrMsg);
	if (rc == SQLITE_OK)
	{
		// if ther are no errors so its good to go
		isAdd = true;
	}

	data.clear();

	return isAdd;
}

std::vector<std::string> DataBase::getBestScores()
{
	std::vector<std::string> usersNames;
	std::vector<std::string> usersScores;
	std::vector<std::string> infoAboutUsers;

	std::string quoted_username;
	int userscore = 0;
	std::string str_userscore;
	std::ostringstream convert;

	std::map<std::string, std::string> mapOfUsersNamesAndScores;

	query = "select username from t_users";

	rc = sqlite3_exec(_db, query.c_str(), callbackScores, 0, &zErrMsg);

	for (unsigned i = 0; i < infoAboutUsersFromCallBack.size(); i++)
	{
		// inserting all the user names to the vector usernsNames
		usersNames.push_back(infoAboutUsersFromCallBack[i]);
	}

	infoAboutUsersFromCallBack.clear();


	for (unsigned i = 0; i < usersNames.size(); i++)
	{
		quoted_username = quoteSign + usersNames[i] + quoteSign;
		query = "select * from t_players_answers where username = " + quoted_username + ";";
		rc = sqlite3_exec(_db, query.c_str(), callbackScores, 0, &zErrMsg);

		// the is_correct starts in [4] and has a jump of 6 steps every time 
		for (unsigned j = 4; j < infoAboutUsersFromCallBack.size(); j += 6)
		{
			if (std::stoi(infoAboutUsersFromCallBack[j]) == 1) // means thet the user answer is correct
			{
				userscore++;
			}
		}

		convert << userscore;
		str_userscore = convert.str();

		userscore = 0; // clear the user score for a new user score
		convert.str("");
		convert.clear(); // clear state flags
		infoAboutUsersFromCallBack.clear();

		usersScores.push_back(str_userscore);
	}


	for (size_t i = 0; i < usersNames.size(); i++)
	{
		mapOfUsersNamesAndScores.insert(std::pair<std::string, std::string>(usersNames[i], usersScores[i]));
	}

	// Declaring the type of Predicate that accepts 2 pairs and return a bool
	typedef std::function<bool(std::pair<std::string, std::string>, std::pair<std::string, std::string>)> Comparator;

	// Defining a lambda function to compare two pairs. It will compare two pairs using second field
	Comparator compFunctor = [](std::pair<std::string, std::string> elem1, std::pair<std::string, std::string> elem2)
	{
		return std::stoi(elem1.second) > std::stoi(elem2.second); // ther is a chance thet it dont need to have stoi
	};

	// Declaring a set that will store the pairs using above comparision logic
	std::set<std::pair<std::string, std::string>, Comparator> setOfUserNamesAndUserScores(mapOfUsersNamesAndScores.begin(), mapOfUsersNamesAndScores.end(), compFunctor);

	// Iterate over a set using range base for loop
	// It will display the items in sorted order of values
	for (std::pair<std::string, std::string> element : setOfUserNamesAndUserScores)
	{
		infoAboutUsers.push_back(element.first); // insert the user name to the vector
		infoAboutUsers.push_back(element.second); // insert the user score to the vector
	}

	return infoAboutUsers;
}

std::vector<std::string> DataBase::getPersonalStatus(std::string username)
{
	std::vector<std::string> personalStatusData; // contains:
												 //	[0] = number of games 
												 //	[1] = number of right answers
												 //	[2] = number of wrong answers
												 //	[3] = average time for answer
	int numOfRightAnswers = 0;
	int numOfWrongAnswers = 0;
	float averageTimeForAnswer = 0;
	int numOfAnswers = 0;

	// convert the string to a string with quotes
	std::string quoted_username = quoteSign + username + quoteSign;


	// by distincting the duplicates we can get the number of games thet the attanted (with count)
	query = "select count(distinct game_id) from t_players_answers where username =" + quoted_username + ";";
	rc = sqlite3_exec(_db, query.c_str(), callbackCount, 0, &zErrMsg);
	personalStatusData.push_back(data); // push back the number of games the user played
	data.clear();


	// get the number of correct answers.
	query = "select count(*) from t_players_answers where username =" + quoted_username + "and is_correct = 1;";
	rc = sqlite3_exec(_db, query.c_str(), callbackCount, 0, &zErrMsg);
	personalStatusData.push_back(data); // push back the number of right answers thet the player has
	data.clear();


	// get the number of worng answers.
	query = "select count(*) from t_players_answers where username =" + quoted_username + "and is_correct = 0;";
	rc = sqlite3_exec(_db, query.c_str(), callbackCount, 0, &zErrMsg);
	personalStatusData.push_back(data); // push back the number of right answers thet the player has
	data.clear();


	// get the avg of the time on al the answers of the user;
	if(personalStatusData[0] == "0") // if the user doeswnt have games
	{
		data = "0";
	}
	else
	{
		query = "select avg(answer_time) from t_players_answers where username =" + quoted_username + ";";
		rc = sqlite3_exec(_db, query.c_str(), callbackCount, 0, &zErrMsg);
	}
	personalStatusData.push_back(data); // push back the number of right answers thet the player has
	data.clear();


	return personalStatusData;
}

int DataBase::callbackCount(void * notUsed, int argc, char ** argv, char ** azCol)
{
	data = *argv; // now data should contain the number of reshumot in a table
	return 0;
}

int DataBase::callbackQuestions(void * notUsed, int argc, char ** argv, char ** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		questionsFromCallBack.push_back(argv[i]);
	}

	return 0;
}

int DataBase::callbackScores(void * notUsed, int argc, char ** argv, char ** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		infoAboutUsersFromCallBack.push_back(argv[i]);
	}
	return 0;
}

/*int DataBase::callbackScores(void *, int, char **, char **)
{
return 0;
}*/
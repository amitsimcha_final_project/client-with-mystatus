#include "Validator.h"

bool Validator::isUsernameValid(string Username)
{
	bool valid = false;

	// check if has first letter.
	if (('a' <= Username[0] && Username[0] <= 'z') ||
		('A' <= Username[0] && Username[0] <= 'Z'))
	{
		valid = true;
	}

	// check if no ' '.
	for (unsigned i = 0; i < Username.length(); i++)
	{
		if (Username[i] == ' ')
		{
			valid = false;
			break;
		}
	}

	// check username isnt empty.
	if (Username.length() == 0)
		valid = false;

	return valid;
}


bool Validator::isPasswordValid(string password)
{
	int numChers = 0, numDigits = 0, numLittleLetters = 0, numBigLetters = 0;

	for (unsigned i = 0; i < password.length(); i++)
	{
		if (password[i] == ' ')
			return false;

		if ('a' <= password[i] && password[i] <= 'z')
		{
			numLittleLetters++;
			numChers++;
		}

		if ('A' <= password[i] && password[i] <= 'Z')
		{
			numBigLetters++;
			numChers++;
		}

		if ('0' <= password[i] && password[i] <= '9')
		{
			numDigits++;
			numChers++;
		}
	}

	if (numChers < 4 || numDigits < 1 || numLittleLetters < 1 || numBigLetters < 1)
		return false;
	
	return true;
}

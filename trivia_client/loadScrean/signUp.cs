﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;

namespace loadScrean
{
    public partial class SignUp : Form
    {
        private byte[] bufferGet;
        private byte[] bufferSet;
        NetworkStream _clientStream;

        public SignUp(NetworkStream clientStream)
        {
            _clientStream = clientStream;
            InitializeComponent();
        }


        private void GoBackButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            this.Close();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            string userNameLen = "";
            string passwordLen = "";
            string emailLen = "";

            // checks if all the text fildes have values
            if ((UserNameTextBox.Text == "") || (PasswordTextBox.Text == "") || (EmailTextBox.Text == ""))
            {
                ErrorLabel.Text = "Something is mising";
            }
            else
            {
                userNameLen = UserNameTextBox.Text.Length.ToString();
                if (userNameLen.Length < 2)
                {
                    userNameLen = "0" + UserNameTextBox.Text.Length;
                }

                passwordLen = PasswordTextBox.Text.Length.ToString();
                if (passwordLen.Length < 2)
                {
                    passwordLen = "0" + PasswordTextBox.Text.Length;
                }

                emailLen = EmailTextBox.Text.Length.ToString();
                if (emailLen.Length < 2)
                {
                    emailLen = "0" + EmailTextBox.Text.Length;
                }

                // create the message to the server with the message code (203)
                string msgToServer = "203" + userNameLen + UserNameTextBox.Text + passwordLen + PasswordTextBox.Text + emailLen + EmailTextBox.Text;

                // send the data about the user to the server
                bufferSet = new ASCIIEncoding().GetBytes(msgToServer);
                _clientStream.Write(bufferSet, 0, msgToServer.Length);
                _clientStream.Flush();

                // get a return value from the server
                bufferGet = new byte[4];
                int bytesRead = _clientStream.Read(bufferGet, 0, 4);
                string answerFromServer = new ASCIIEncoding().GetString(bufferGet);


                if (answerFromServer == "1040")
                {
                    // say welcome to the new user
                    var welcomeSound = new System.Media.SoundPlayer(loadScrean.Properties.Resources.welcome_sound);
                    welcomeSound.Play();

                    UserNameTextBox.Text = "";
                    PasswordTextBox.Text = "";
                    EmailTextBox.Text = "";

                    this.Hide();
                    this.Close();
                }
                else if (answerFromServer == "1041")
                {
                    ErrorLabel.Text = "Pass illegal";
                }
                else if (answerFromServer == "1042")
                {
                    ErrorLabel.Text = "Username is already exists";
                }
                else if (answerFromServer == "1043")
                {
                    ErrorLabel.Text = "Username is illegal";
                }
                else if (answerFromServer == "1044")
                {
                    ErrorLabel.Text = "Other";
                }
            }
        }

        private void SignUp_Load(object sender, EventArgs e)
        {

        }
    }
}
